$(document).ready(function() {
  $('.togglebar').click(function(){
    $('nav').toggle();
    $(this).toggleClass('crossbutton');
  });
              
    $('.owl-carousel').owlCarousel({
                loop: true,
                margin: 10,
                responsiveClass: true,
                navText : ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
                responsive: {
                  0: {
                    items: 1,
                    nav: false
                  },
                  600: {
                    items: 1,
                    nav: false
                  },
                  1000: {
                    items: 1,
                    nav: true,
                    loop: false,
                    margin: 20
                  }
                }
});

  $('.clickvideo').click(function(){
    $('.modal-popup').addClass('modal-popup-visible');
  });

});

const closeModal = () => {
  $('.modal-popup').removeClass('modal-popup-visible');
}